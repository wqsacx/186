#pragma once
#include <string>
#include <vector>
#include "common.h"

namespace window
{
    class monitor;
    extern double deltaTime;
    extern float fps;
    void createWindow(const vec2i32 &size, const std::string &name);
    void createWindow(const vec2i32 &size, const std::string &name, const monitor*);

    void setSizeBounds(const vec2i32 &minSize, const vec2i32 &maxSize);
    void setAspectRatio(const vec2i32 &aspect);

    void setWindowed();

    void swapBuffers();
    void pollEvents();
    void closeWindow();
    void setShouldClose(bool);
    vec2i32 getSize();
    void setSize(const vec2i32&);
    bool shouldClose();
    void setTitle(const std::string &title);

    void initMonitors();

    #ifdef _glfw3_h_
    GLFWwindow *getHandler();
    #endif // _glfw3_h_

    class monitor
    {
    public:
        monitor(void*);

        const vec2u32 &getNativeMode() const;

        void bind() const;
        void unbind() const;

        std::string name;
    private:
        std::vector<vec2u32> _modes;
        vec2u32 _nativeMode;

        void *handle;
    public:
        const vec2u32 &operator[](uint32_t a) const
        {
            return _modes.at(a);
        }

        uint32_t count() const
        {
            return _modes.size();
        }
    };

    class cursor
    {
    public:
        cursor();
        ~cursor();

        void create(const std::string &path, const vec2i32 &offset);
        void bind();

        void _createDefault(uint32_t);
    private:
        void *_cursor;
    };

    extern const std::vector<monitor> &monitors;
    extern cursor defaultCursor, handCursor;
}
