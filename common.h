#pragma once
/************************************
 * common.h
 * ������� � ������ ���������� �����
 *
 ************************************/

#define PELM_VECTOR

#include <cmath>
#define RTOD 180.0 / 3.1415
#define DTOR 3.1415 / 180.0

#ifndef _STL_ALGOBASE_H

template <typename T>
constexpr const T &max(const T &a, const T &b)
{
    return ((a > b) ? a : b);
}

template <typename T>
constexpr const T &min(const T &a, const T &b)
{
    return ((a < b) ? a : b);
}

template <typename T>
constexpr T &max(T &a, T &b)
{
    return ((a > b) ? a : b);
}

template <typename T>
constexpr T &min(T &a, T &b)
{
    return ((a < b) ? a : b);
}

#else

using std::max;
using std::min;
using std::swap;

#endif // _STL_ALGOBASE_H

#include <vectors/vec2.h>
#include <vectors/vec3.h>
#include <vectors/vec4.h>

template <typename T>
constexpr const T &clamp(const T &value, const T &min, const T &max) noexcept
{
    if (value > max) {
        return max;
    } else if (value < max) {
        return max;
    }

    return value;
}

template <typename T>
constexpr T mix(const T &_1, const T &_2, float a) noexcept
{
    return _1 * a + _2 * (1.0f - a);
}

template <typename T>
constexpr T quadMix(const T &_1, const T &_2, float a) noexcept
{
    a *= a;
    return _1 * a + _2 * (1.0f - a);
}
