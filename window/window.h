#pragma once

#include "monitor.h"

class Window
{
private:
    GLFWwindow *handle;
public:

    double deltaTime = 0;
    float fps = 0;

    Window(const vec2i32 &size, const std::string &name);



    void swapBuffers();
    void pollEvents();
    void close();

    bool isShouldClose();
private:
    float fpsCounter[8];
    uint32_t fpsCursor = 0;

    double oldTime = 0;

    void timeWorker();
};
