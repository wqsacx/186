#include "window.h"
#include <GLFW/glfw3.h>
#include <cstring>

Window::Window(const vec2i32 &size, const std::string &name)
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_REFRESH_RATE, 60);

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 16);
    glfwWindowHint(GLFW_RESIZABLE, false);

    this->handle = glfwCreateWindow(size.x, size.y, name.c_str(), nullptr, nullptr);


    memset(this->fpsCounter, 0, 8 * sizeof (float));
}

void Window::timeWorker()
{
    double currentTime = glfwGetTime();

    this->deltaTime = this->oldTime - currentTime;
    this->oldTime = currentTime;

    this->fpsCounter[this->fpsCursor++] = 1.0f / this->deltaTime;
    if (this->fpsCursor == 8) {
        this->fpsCursor = 0;
    }

    this->fps = 0;
    for(uint32_t i = 0; i < 8; i++) {
        fps += this->fpsCounter[i];
    }

    this->fps /= 8.0f;
}

void Window::swapBuffers()
{
    this->timeWorker();
    glfwSwapBuffers(this->handle);
}

void Window::close()
{
    glfwDestroyWindow(this->handle);
}


