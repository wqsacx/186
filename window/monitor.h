#pragma once

#include <string>
#include <vector>
#include <GLFW/glfw3.h>
#include "../common.h"

class Monitor
{
public:
    Monitor(GLFWmonitor*);

    const vec2u32 &getNativeMode() const;

    void bind() const;
    void unbind() const;

    std::string name;
private:
    std::vector<vec2u32> _modes;
    vec2u32 _nativeMode;

    void *handle;
public:
    const vec2u32 &operator[](uint32_t a) const
    {
        return _modes.at(a);
    }

    uint32_t count() const
    {
        return _modes.size();
    }
};
