#include "monitor.h"


Monitor::Monitor(void *m)
{
    GLFWmonitor *mon = (GLFWmonitor*)m;
    handle = m;
    const GLFWvidmode *nativeMode = glfwGetVideoMode(mon);
    _nativeMode.x = nativeMode->width;
    _nativeMode.y = nativeMode->height;
    int count;

    const GLFWvidmode *_vm = glfwGetVideoModes(mon, &count);
    for(int i = 0; i < count; i++)
        _modes.push_back(vec2u32(_vm[i].width, _vm[i].height));
    name = glfwGetMonitorName(mon);
}

void monitor::bind() const
{
    glfwSetWindowMonitor(::handle, (GLFWmonitor*)handle, 0, 0, _nativeMode.x, _nativeMode.y, 0);
}

void monitor::unbind() const
{
    glfwSetWindowMonitor(::handle, 0, 0, 0, _nativeMode.x, _nativeMode.y, 0);
}